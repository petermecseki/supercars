package thesis.supercars;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import thesis.supercars.persistence.domain.Car;
import thesis.supercars.persistence.repository.CarRepository;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;


@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class CarRepositoryTest {

    @Autowired
    private CarRepository carRepository;

    @Test
    public void mapping() {
        Car savedCar = this.carRepository.save(
                new Car("Ford",
                        "Focus",
                        "Sedan",
                        22000,
                        new Date(2015 - 05 - 01),
                        "Metal Blue", 6500000,
                        "Szép állapot"));

        Car car = this.carRepository.getOne(savedCar.getId());

        assertThat(car.getBodyType()).isEqualTo("Sedan");
        assertThat(car.getFirstRegistration()).isEqualToIgnoringHours(new Date(2015 - 05 - 01));
        assertThat(car.getId()).isNotNull();
        assertThat(car.getId()).isGreaterThan(0);
    }
}
