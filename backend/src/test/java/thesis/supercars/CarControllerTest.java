package thesis.supercars;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import thesis.supercars.controller.CarController;
import thesis.supercars.persistence.domain.Car;
import thesis.supercars.persistence.repository.CarRepository;
import thesis.supercars.persistence.repository.FeatureRepository;
import thesis.supercars.service.CarService;

import java.util.Date;
import java.util.Optional;

import static org.mockito.BDDMockito.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = CarController.class, secure = false)
@AutoConfigureTestDatabase(replace = NONE)
@RunWith(SpringRunner.class)
public class CarControllerTest {

    @MockBean
    CarRepository carRepository;
    @MockBean
    CarService carService;
    @MockBean
    FeatureRepository featureRepository;
    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MockMvc mockMvc;

    @Before
    public void setUp() {
        Car testCar = new Car("Ford",
                "Focus",
                "Sedan",
                22000,
                new Date(2015 - 05 - 01),
                "Metal Blue", 6500000,
                "Szép állapot");

        given(carRepository.findById(1L)).willReturn(Optional.of(testCar));
        given(carRepository.findById(2L)).willReturn(Optional.empty());
        given(carRepository.save(any(Car.class))).willReturn(testCar);
        doNothing().when(carRepository).deleteCarById(any(Long.class));
    }

    @Test
    public void testGetCarById() throws Exception {

        this.mockMvc.perform(
                get("/public/car/{id}", 1L)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.model").value("Focus"));

        verify(carRepository, times(1)).findById(any(Long.class));
        verifyNoMoreInteractions(carRepository);
    }

    @Test
    public void testGetCarByIdNotFound() throws Exception {

        this.mockMvc.perform(
                get("/public/car/{id}", 2L)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(carRepository, times(1)).findById(any(Long.class));
        verifyNoMoreInteractions(carRepository);
    }

    @Test
    public void testSave() throws Exception {

        this.mockMvc
                .perform(
                        post("/public/addcar")
                                .content(this.objectMapper.writeValueAsBytes(
                                        new Car("Ford",
                                                "Focus",
                                                "Sedan",
                                                22000,
                                                new Date(2015 - 05 - 01),
                                                "Metal Blue", 6500000,
                                                "Szép állapot")))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        verify(carRepository, times(1)).save(any(Car.class));
        verifyNoMoreInteractions(carRepository);
    }

    @Test
    public void testUpdate() throws Exception {

        this.mockMvc
                .perform(
                        put("/public/car/{id}", 1L)
                                .content(this.objectMapper.writeValueAsBytes(new Car("Ford",
                                        "Focus",
                                        "Sedan",
                                        22000,
                                        new Date(2015 - 05 - 01),
                                        "Metal Blue", 6500000,
                                        "Szép állapot")))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(carRepository, times(1)).save(any(Car.class));
        verifyNoMoreInteractions(carRepository);
    }

    @Test
    public void testDelete() throws Exception {

        this.mockMvc
                .perform(
                        delete("/public/car/{id}", 1L))
                .andExpect(status().isNoContent());
        verify(carRepository, times(1)).deleteCarById(any(Long.class));
        verifyNoMoreInteractions(carRepository);
    }
}
