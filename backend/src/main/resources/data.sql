INSERT INTO "user" (username,first_name,last_name,email,phone_number) VALUES ('Killer', 'John', 'Doe', 'johnthedoe@gmail.com','06701264952');
INSERT INTO "user" (username,first_name,last_name,email,phone_number) VALUES ('Rotten', 'Harley', 'Queen', 'mrsqueen@gmail.com','06201245622');
INSERT INTO car(brand,model,body_type,color,first_registration,kilometer,price,details) VALUES ('AUDI','A6','Sedan','Fekete', DATE '2011-10-20',20000,6000000,'Nagyon szép állapotban, garázsban tartott női tulajtól :)');
INSERT INTO car(brand,model,body_type,color,first_registration,kilometer,price) VALUES ('AUDI','S8','Sedan','Fekete', DATE '2010-10-13',12000,15000000);
INSERT INTO car(brand,model,body_type,color,first_registration,kilometer,price) VALUES ('BMW','M5','Coupe','Ezüst',DATE '2015-01-01',50000,10000000);
INSERT INTO car(brand,model,body_type,color,first_registration,kilometer,price) VALUES ('BMW','X6','Coupe','Fekete',DATE '2016-06-08',3000,12000000);

INSERT INTO engine(engine_capacity,power,fuel_type, car_id) VALUES (2983, 233,'Dizel',1);
INSERT INTO engine(engine_capacity, power,fuel_type, car_id) VALUES (4860,380,'Benzin',2);
INSERT INTO engine(engine_capacity, power,fuel_type, car_id) VALUES (5900,540,'Hibrid',3);
INSERT INTO engine(engine_capacity, power,fuel_type, car_id) VALUES (2490,290,'Benzin',4);

INSERT INTO address (post_code,city, street_address) VALUES (7300,'Győr', 'Plaza u. 10.');
INSERT INTO feature(name) VALUES ('abs');
INSERT INTO feature(name) VALUES ('asr');
INSERT INTO feature(name) VALUES ('esp');
INSERT INTO feature(name) VALUES ('warranty');
INSERT INTO feature(name) VALUES ('service_history');
INSERT INTO feature(name) VALUES ('tempomat');
INSERT INTO feature(name) VALUES ('xenon');

INSERT INTO car_feature(car_id,feature_id) VALUES (1,1);
INSERT INTO car_feature(car_id,feature_id) VALUES (2,1);
INSERT INTO car_feature(car_id,feature_id) VALUES (3,2);
INSERT INTO car_feature(car_id,feature_id) VALUES (2,2);
INSERT INTO car_feature(car_id,feature_id) VALUES (3,1);

INSERT INTO saved_car(user_id, car_id) VALUES (3,1);
INSERT INTO saved_car(user_id, car_id) VALUES (1,2);
INSERT INTO saved_car(user_id, car_id) VALUES (2,1);
INSERT INTO saved_car(user_id, car_id) VALUES (2,3);

INSERT INTO role(name) VALUES('ROLE_USER');
INSERT INTO role(name) VALUES('ROLE_ADMIN');