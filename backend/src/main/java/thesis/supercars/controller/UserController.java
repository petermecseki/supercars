package thesis.supercars.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import thesis.supercars.payload.UserIdentityAvailability;
import thesis.supercars.persistence.repository.UserRepository;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/checkUsernameAvailable")
    public UserIdentityAvailability checkUsernameAvailability(@RequestParam(value = "username") String username) {
        Boolean usernameIsAvailable = !userRepository.existsByUsername(username);
        return new UserIdentityAvailability(usernameIsAvailable);
    }

    @GetMapping("/checkEmailAvailable")
    public UserIdentityAvailability checkEmailAvailability(@RequestParam(value = "email") String email) {
        Boolean emailIsAvailable = !userRepository.existsByEmail(email);
        return new UserIdentityAvailability(emailIsAvailable);
    }
}