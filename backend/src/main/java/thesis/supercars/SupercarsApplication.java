package thesis.supercars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupercarsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SupercarsApplication.class, args);
	}
}
