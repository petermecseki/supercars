package thesis.supercars.persistence.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import thesis.supercars.persistence.domain.Car;
import thesis.supercars.persistence.domain.Feature;

import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    Car findCarById(Long id);

    List<Car> findCarsByBrandAndModel(Pageable pageable, @Param("brand") String brand, @Param("model") String model);

    List<Car> findCarsByBrand(String brand);


    Page<Car> findCarsByBrand(Pageable pageable,String brand);

    @Query("SELECT car FROM Car car WHERE lower(car.brand) LIKE lower(:brand)")
    Page<Car> getCarsByBrand(@Param("brand") String brand, Pageable pageable);

    @Query("SELECT DISTINCT car.brand FROM Car car")
    List<String> getAllCarBrands();

    @Query("SELECT DISTINCT car.model FROM Car car WHERE lower(car.brand) LIKE lower(:brand)")
    List<String> getAllModelsByBrand(@Param("brand") String brand);

    @Query("SELECT car FROM Car car WHERE lower(car.brand) LIKE lower(:brand) AND lower(car.model) LIKE lower(:model)")
    List<Car>getCarsByFilter(@Param("brand") String brand, @Param("model")String model);

    List<Car> findCarsByFeatures(List<Feature> features);

    void deleteCarById(Long id);
}