package thesis.supercars.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import thesis.supercars.persistence.domain.ImageFile;

public interface ImageFileRepository extends JpaRepository<ImageFile, String> {
}
