package thesis.supercars.persistence.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
public class Engine implements Serializable {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private int engineCapacity;
    private int power;

    @Size(max = 20)
    private String fuelType;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonBackReference
    private Car car;

    private Engine() {

    }

    public Engine(int engineCapacity, int power, String fuelType, Car car) {
        this.engineCapacity = engineCapacity;
        this.power = power;
        this.fuelType = fuelType;
        this.car = car;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(int engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    @Override
    public String toString() {
        return "Engine{" +
                "id=" + id +
                ", engineCapacity=" + engineCapacity +
                ", power=" + power +
                ", fuelType='" + fuelType +
                '}';
    }
}
