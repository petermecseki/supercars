package thesis.supercars.persistence.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class SavedCar implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private User user;

    @OneToOne
    private Car car;

    public SavedCar() {

    }

    public SavedCar(User user, Car car) {
        this.user = user;
        this.car = car;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    @Override
    public String toString() {
        return "SavedCar{" +
                "id=" + id +
                '}';
    }
}
