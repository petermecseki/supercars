package thesis.supercars.persistence.domain;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
public class Address implements Serializable {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    private int postCode;

    @Size(max = 30)
    private String city;

    @Size(max = 50)
    private String streetAddress;

    @OneToOne
    private User user;

    private Address() {

    }

    public Address(int postCode, String city, String streetAddress, User user) {
        this.postCode = postCode;
        this.city = city;
        this.streetAddress = streetAddress;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getPostCode() {
        return postCode;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", postCode=" + postCode +
                ", city='" + city + '\'' +
                ", streetAddress='" + streetAddress + '\'' +
                ", user=" + user +
                '}';
    }
}
