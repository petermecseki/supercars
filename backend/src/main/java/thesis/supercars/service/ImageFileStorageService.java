package thesis.supercars.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import thesis.supercars.exception.ImageFileStorageException;
import thesis.supercars.persistence.domain.ImageFile;
import thesis.supercars.persistence.repository.ImageFileRepository;

import java.io.IOException;

@Service
public class ImageFileStorageService {

    @Autowired
    private ImageFileRepository imageFileRepository;

    public void setImageFileRepository(ImageFileRepository imageFileRepository) {
        this.imageFileRepository = imageFileRepository;
    }

    public ImageFile storeFile(MultipartFile file) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            if (fileName.contains("..")) {
                throw new ImageFileStorageException("Filename contains invalid path sequence " + fileName);
            }
            ImageFile ImageFile = new ImageFile(fileName, file.getContentType(), file.getBytes());
            return imageFileRepository.save(ImageFile);
        } catch (IOException ex) {
            throw new ImageFileStorageException("Could not store file " + fileName + ". Try again please!", ex);
        }
    }

    public ImageFile getFile(String fileId) {
        return imageFileRepository.findById(fileId)
                .orElseThrow(() -> new ImageFileStorageException("File not found with id " + fileId));
    }
}

