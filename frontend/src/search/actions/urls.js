export const GET_ALL_CARS_URL = 'http://localhost:16000/public/cars';
export const GET_ALL_CAR_BRANDS_URL = 'http://localhost:16000/public/brands';
export const GET_MODELS_BY_BRAND_URL = 'http://localhost:16000/public/models/';
export const FILTER_CARS_URL = 'http://localhost:16000/public/filter'
export const GET_CAR_DETAILS_URL = 'http://localhost:16000/public/car/';
