import React, {Component} from 'react';
import {connect} from 'react-redux';

import {setActivePage} from '../actions/index';

class Pager extends Component {

    constructor(props) {
        super(props);

        this.state = {
            numberOfPages: 1
        }

        this.onClickPreviousBtn = this.onClickPreviousBtn.bind(this);
        this.onClickNextBtn = this.onClickNextBtn.bind(this);
    }

    componentDidMount() {
        this.calculateNumberOfPages();
    }

    calculateNumberOfPages() {
        const numberOfPages = Math.ceil(this.props.carsCount / this.props.boxesPerPage);

        this.setState({
            numberOfPages: numberOfPages > 1 ? numberOfPages : this.state.numberOfPages
        });
    }

    onChangePage(value) {
        this.changeActivePage(parseInt(value));
    }

    getPagerItemStyle(itemNumber) {
        return this.props.activePage == itemNumber ? 'page-item active' : 'page-item';
    }

    getPageNumbers() {
        let numbers = [];

        for (var i = 1; i <= this.state.numberOfPages; i++) {
            const activeOrNot = this.getPagerItemStyle(i);
            numbers.push(
                <li className={activeOrNot} key={i} onClick={(event) => this.onChangePage(event.target.innerText)}>
                    <span className='page-link'>{i}</span>
                </li>
            );
        }

        return numbers;
    }

    getNextBtnStyle() {
        return this.props.activePage == this.state.numberOfPages ? 'page-item disabled' : 'page-item';
    }

    getPreviousBtnStyle() {
        return this.props.activePage == 1 ? 'page-item disabled' : 'page-item';
    }

    changeActivePage(page) {
        this.props.setActivePage(page);
       // this.props.refreshCourses(page);
    }

    onClickPreviousBtn() {
        const activePage = this.props.activePage;

        if (activePage > 1) {
            this.changeActivePage(activePage - 1);
        }
    }

    onClickNextBtn() {
        const activePage = this.props.activePage;

        if (activePage < this.state.numberOfPages) {
            this.changeActivePage(activePage + 1);
        }
    }

    getPreviousBtn() {
        return (
            <li className={this.getPreviousBtnStyle()}
                onClick={this.onClickPreviousBtn}>
                <span className="page-link">Előző</span>
            </li>
        );
    }

    getNextBtn() {
        return (
            <li className={this.getNextBtnStyle()}
                onClick={this.onClickNextBtn}>
                <span className="page-link">Következő</span>
            </li>
        );
    }

    render() {
        if (this.props.carsCount && this.props.activePage) {
            return (
                <div className='col'>
                    <nav>
                        <ul className="pagination justify-content-center">
                            {this.getPreviousBtn()}
                            {this.getPageNumbers()}
                            {this.getNextBtn()}
                        </ul>
                    </nav>
                </div>
            );
        }
    }

}

function mapStateToProps(state) {
    return {
        carsCount: state.cars.allCount,
        activePage: state.cars.activePage
    }
}

export default connect(mapStateToProps, {setActivePage})(Pager);