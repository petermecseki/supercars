import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';


import moment from 'moment';
import {faHeart} from "@fortawesome/free-solid-svg-icons";

export default class CarBox extends Component {

    constructor(props) {
        super(props);

        this.state = {
            favourites: [],
            selectedFavourite: null
        }

        this.updateFavourites = this.updateFavourites.bind(this);
    }


    updateFavourites(event) {

        console.log("Update favourites: " + event);
        this.setState({
            selectedFavourite: event
        });


        if (this.state.favourites.includes(event)) {
            const i = this.state.favourites.indexOf(event);
            if (i !== -1) {
                this.state.favourites.splice(i, 1);
            }
        } else {
            this.state.favourites.push(event);
        }
    }

    render() {
        const url = '/car/' + this.props.carId;
        const firstRegistration = moment(this.props.firstRegistration);
        const kilometers = new Intl.NumberFormat('en-IN', {style: 'decimal'}).format(this.props.kilometer).replace(',', ' ');
        const prize = new Intl.NumberFormat('hu-HU', {style: 'decimal'}).format(this.props.prize);

        return (
            <div className="row box mt-2 mb-2 rounded" key={this.props.carId}>
                <div className="col-12 col-md-3 p-2">
                    <img
                        src='https://hips.hearstapps.com/amv-prod-cad-assets.s3.amazonaws.com/images/15q4/662479/2016-aston-martin-db9-gt-first-drive-review-car-and-driver-photo-662971-s-original.jpg'
                        className='img-fluid'/>
                </div>
                <div className="col-12 col-md pt-1 pb-0 pl-2 pr-2">
                    <Link to={url}><h1>{this.props.carType}</h1></Link>
                    <p>
                        <span>{firstRegistration.format('YYYY')}/{firstRegistration.format('M')}</span>
                        <span>{this.props.engine.engineCapacity} cm<sup>3</sup></span>
                        <span>{this.props.engine.fuelType}</span>
                        <span>{this.props.engine.power} LE</span>
                        <span>{kilometers} km</span>
                    </p>
                    <p>
                        {this.props.description}
                    </p>
                </div>
                <div className="col-12 col-md-2 pt-1 pl-2 pl-md-0 pr-2 pb-2 pb-md-0 text-left text-md-right">
                    {prize} Ft
                </div>
                <div className="col-12 col-md-2 pt-2 pl-2 pl-md-0 pr-2 pb-2 pb-md-0 text-left text-md-right">
                    <button key={this.props.carId} value={this.props.carId}
                            onClick={() => this.updateFavourites(this.props.carId)}>
                        <FontAwesomeIcon size="3x" icon={faHeart}/>
                    </button>
                </div>
            </div>
        );
    }
}