import { get, post } from 'axios';

import { REGISTRATION_URL, LOGIN_URL } from "./urls";

export function registration (registrationData) {
    return async dispatch => {
        return new Promise(function (resolve, reject) {
            post(
                REGISTRATION_URL,
                registrationData,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            ).then(response => {
                console.log(response.data);
                return resolve(response.data);
            }).catch((error) => {
                return reject(error);
            });
        });
    }
}

export function login(loginData) {
    return async dispatch => {
        return new Promise(function (resolve, reject) {
            post(
                LOGIN_URL,
                loginData,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            ).then(response => {
                return resolve(response.data);
            }).catch((error) => {
                return reject(error);
            });
        });
    }
}