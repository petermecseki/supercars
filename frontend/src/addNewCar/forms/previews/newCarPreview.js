import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

class NewCarPreview extends PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            
        }
    }

    changeValue(value) {
        
    }

    render() {
        return (
            <div className='form-group'>
                { this.props.carTitle }
            </div>
        );
    }

}

function mapStateToProps(state) {
    return {
        carTitle: state.addNewCar.carTitle
    }
}

export default connect(mapStateToProps, {})(NewCarPreview);