import React, { PureComponent } from 'react';

export default class SelectField extends PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            values: this.props.values,
            selectedValue: this.props.values[0]
        }
    }

    changeValue(value) {
        this.setState({
            value
        });
    }

    getField() {
        return (
            <select className="form-control">
                {
                    this.state.values.map(value => {
                        return <option value={value} key={value}>{value}</option>
                    })
                }
            </select>
        );
    }

    render() {
        return (
            <div className='form-group row mt-4'>
                <div className='col-sm-2'>
                    <label htmlFor={this.props.name}>{this.props.label}</label>
                </div>
                <div className='col-sm-10'>
                    {this.getField()}
                </div>
            </div>
        );
    }

}