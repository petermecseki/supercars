import React, { PureComponent } from 'react';

export default class Button extends PureComponent {

    render() {
        return (
            <div className='form-group mt-4'>
                <button type="submit" className="btn btn-primary mb-2">Feltöltés</button>
            </div>
        );
    }

}