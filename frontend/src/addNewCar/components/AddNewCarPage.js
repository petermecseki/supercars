import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import SelectField from '../forms/fields/selectField';
import CheckBoxFields from '../forms/fields/checkBoxFields';
import NewCarPreview from '../forms/previews/newCarPreview';

import { setCarTitle } from '../../search/actions';
import TextAreaField from '../forms/fields/textAreaField';
import Button from '../forms/fields/button';
import NumberInputField from '../forms/fields/numberInputField';
import CarImageDropField from '../forms/fields/carImageDropField';

class AddNewCarPage extends Component {

    constructor(props) {
        super(props);

        const cars = ['Audi', 'Ford', 'Aston Martin'];
        const carTypes = ['A5', 'A3'];
        const features = ['digitális klíma', 'automata váltó', 'tolató kamera', 'ABS'];
        this.state = {
            cars,
            features,
            carTypes
        }

        this.onChangeCarTitle = this.onChangeCarTitle.bind(this);
    }

    onChangeCarTitle(title) {
        this.props.setCarTitle(title);
    }

    onChangeCarPrize(prize) {
        console.log("prize: ", prize);
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        <form>
                            <div className="border p-2">
                                <h1 className="p-0 m-0">Hirdetés előnézete</h1>
                                <NewCarPreview />
                            </div>
                            <h1 className="mt-4 mb-4">Új autó feltöltése</h1>
                            <SelectField name='car-brand' label='Autó márkája' values={this.state.cars} />
                            <SelectField name='car-type' label='Autó típusa' values={this.state.carTypes} />
                            <NumberInputField name='car-prize' label='Ár (HUF)' onChange={this.onChangeCarPrize} />
                            <TextAreaField name='car-desciption' label='Leírás' onChange={this.onChangeCarTitle} />
                            <CheckBoxFields name='fetures' label='Felszereltség' values={this.state.features} />
                            <CarImageDropField />
                            <Button />
                        </form>
                    </div>
                </div>
            </div>
        );
    }

}

export default connect(null, { setCarTitle })(AddNewCarPage);