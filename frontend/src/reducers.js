import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';

import carReducer from './search/reducers';
import carDetailsReducer from './carDetails/reducers';
import addNewCarReducer from './addNewCar/reducers/addNewCar';

export default combineReducers({
    routing: routerReducer,
    cars: carReducer,
    car: carDetailsReducer,
    addNewCar: addNewCarReducer
});
