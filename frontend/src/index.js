import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, compose} from 'redux';
import {BrowserRouter} from 'react-router-dom';
import thunkMiddleware from 'redux-thunk';
import {useRouterHistory} from 'react-router';
import createHistory from 'history/createBrowserHistory';
import {routerMiddleware, ConnectedRouter} from 'react-router-redux';

import {createDevTools} from 'redux-devtools';
import LogMonitor from 'redux-devtools-log-monitor';
import DockMonitor from 'redux-devtools-dock-monitor';

import '../style/basic.scss';
import App from './app';

const browserHistory = createHistory({basename: '/'});
const myRouterMiddleware = routerMiddleware(browserHistory);

import Reducers from './reducers';

import 'regenerator-runtime/runtime';

let middlewares;
let DevTools;

if (process.env.NODE_ENV === 'development') {
    DevTools = createDevTools(
        <DockMonitor toggleVisibilityKey="ctrl-h" changePositionKey="ctrl-q" defaultIsVisible={false}>
            <LogMonitor theme="tomorrow" preserveScrollTop={false}/>
        </DockMonitor>
    );

    middlewares = compose(
        applyMiddleware(thunkMiddleware, myRouterMiddleware),
        DevTools.instrument()
    );
} else {
    middlewares = applyMiddleware(thunkMiddleware, myRouterMiddleware);
}

const store = createStore(
    Reducers,
    middlewares
);

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={browserHistory}>
            <App>
                <div>
                    {DevTools ? <DevTools/> : null}
                </div>
            </App>
        </ConnectedRouter>
    </Provider>
    , document.querySelector('#root'));

export default browserHistory;