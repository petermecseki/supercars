import React, {PureComponent} from 'react';

export default class NumberInputField extends PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            value: ''
        }
    }

    changeValue(value) {
        this.setState({
            value
        });
        this.props.onChange(value);
    }

    getField() {
        return (
            <input value={this.state.value}
                   type="number"
                   className="form-control"
                   id={this.props.name}
                   onChange={(event) => this.changeValue(event.target.value)}/>
        );
    }

    render() {
        return (
            <div className='form-group row mt-4'>
                <div className='col-sm-2'>
                    <label htmlFor={this.props.name}>{this.props.label}</label>
                </div>
                <div className='col-sm-10'>
                    {this.getField()}
                </div>
            </div>
        );
    }

}