import React, {Component} from 'react';

import _ from 'lodash';

export default class SelectField extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedValue: null
        }
    }

    componentDidMount() {
        this.setState({
            selectedValue: null
        });
    }

    changeValue(value) {
        let selectedValue = value;

        if (value === '- Kérem válasszon -') {
            selectedValue = null;
        }

        this.props.change(selectedValue);
        this.setState({
            selectedValue
        });
    }

    getField() {
        return (
            <select className="form-control" onChange={(event) => this.changeValue(event.target.value)}>
                <option value={null} key={"empty-" + this.props.name} defaultValue>- Kérem válasszon -</option>
                {
                    !_.isNull(this.props.values) && this.props.values.map(value => {
                        const isSelected = this.state.selectedValue === value;
                        if (isSelected) {
                            return <option value={value} key={value} defaultValue>{value}</option>
                        } else {
                            return <option value={value} key={value}>{value}</option>
                        }
                    })
                }
            </select>
        );
    }

    render() {
        return (
            <div className='form-group row mt-4'>
                <label htmlFor={this.props.name} className="col-sm-2 col-form-label">{this.props.label}</label>
                <div className='col-sm-10'>
                    {this.getField()}
                </div>
            </div>
        );
    }

}