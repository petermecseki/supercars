import React, {PureComponent, Fragment} from 'react';
import {connect} from 'react-redux';

import SelectField from './selectField';

import {getAllCarBrands, getModelsByBrand, setSelectedBrandAndModel} from '../../../search/actions/index';

class DoubleSelectField extends PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            cars: null,
            carTypes: null,
            selectedCarBrand: null,
            selectedCarModel: null
        }

        this.selectCarBrand = this.selectCarBrand.bind(this);
        this.selectCarModel = this.selectCarModel.bind(this);
    }

    componentDidMount() {
        this.props.getAllCarBrands();
    }

    selectCarBrand(carBrand) {
        this.setState({
            selectedCarBrand: carBrand,
            selectedCarModel: null
        });
        this.props.getModelsByBrand(carBrand);
        this.props.setSelectedBrandAndModel({brand: carBrand, model: null});
    }

    selectCarModel(carModel) {
        this.setState({
            selectedCarModel: carModel
        });
        this.props.setSelectedBrandAndModel({brand: this.state.selectedCarBrand, model: carModel});
    }

    render() {
        return (
            <Fragment>
                {this.props.brands &&
                <SelectField name='car-brand' label='Márka' values={this.props.brands} change={this.selectCarBrand}/>}
                {this.props.models &&
                <SelectField name='car-type' label='Típus' values={this.props.models} change={this.selectCarModel}/>}
            </Fragment>
        );
    }

}

function mapStateToProps(state) {
    return {
        carImages: state.addNewCar.carImages,
        brands: state.cars.brands,
        models: state.cars.models
    }
}

export default connect(mapStateToProps, {
    getAllCarBrands,
    getModelsByBrand,
    setSelectedBrandAndModel
})(DoubleSelectField);