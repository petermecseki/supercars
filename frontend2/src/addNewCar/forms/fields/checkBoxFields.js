import React, {PureComponent} from 'react';
import connect from "react-redux/es/connect/connect";
import {changeFeaturesValues} from "../../../search/actions";

class CheckBoxField extends PureComponent {

    constructor(props) {
        super(props);

        this.state = {}
    }

    changeValue(value) {
        this.props.changeFeaturesValues(value);
        this.setState({});
    }

    getFields() {
        return this.props.values.map(value => {
            return (
                <div className='form-check' key={value.id}>
                    <input type="checkbox" className='form-check-input' id={value.id} name={value.id} value={value}
                           onChange={() => this.changeValue(value)}/>
                    <label htmlFor={value.id} className='form-check-label'>{value.name}</label>
                </div>
            );
        })
    }

    render() {
        return (
            <div className='form-group row mt-4'>
                <div className='col-sm-2'>
                    <h2 className='mb-4'>{this.props.label}</h2>
                </div>
                <div className='col-sm-10'>
                    {this.getFields()}
                </div>
            </div>
        );
    }

}

function mapStateToProps(state) {
    return {}
}

export default connect(mapStateToProps, {changeFeaturesValues})(CheckBoxField);