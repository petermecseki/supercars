import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';

import {addCarImage, removeCarImage, setCarPrize, setCarDescription} from '../actions/index';

import {AuthenticatorContext} from '../../login/components/Authenticator';

import DoubleSelectField from '../forms/fields/doubleSelectField';
import CheckBoxFields from '../forms/fields/checkBoxFields';
import TextAreaField from '../forms/fields/textAreaField';
import Button from '../forms/fields/button';
import NumberInputField from '../forms/fields/numberInputField';
import CarImageDropField from '../forms/fields/carImageDropField';


class AddNewCarPage extends Component {
    static contextType = AuthenticatorContext;

    constructor(props) {
        super(props);

        const cars = ['Audi', 'Ford', 'Aston Martin'];
        const carTypes = ['A5', 'A3'];
        const features = ['digitális klíma', 'automata váltó', 'tolató kamera', 'ABS'];
        const imagePreviews = [];

        this.state = {
            cars,
            features,
            carTypes,
            imagePreviews
        }

        this.addImageToPreviews = this.addImageToPreviews.bind(this);
        this.onChangeCarPrize = this.onChangeCarPrize.bind(this);
        this.onChangeCarDescription = this.onChangeCarDescription.bind(this);
    }

    onChangeCarPrize(prize) {
        this.props.setCarPrize(prize);
    }

    onChangeCarDescription(description) {
        this.props.setCarDescription(description);
    }

    addImageToPreviews(image) {
        const imagePreviews = this.state.imagePreviews;

        if (imagePreviews.length > 0) {
            const imageInPreviews = imagePreviews.filter(img => img.name === image.name);

            if (imageInPreviews.length === 0) {
                imagePreviews.push(image);
            }
        } else {
            imagePreviews.push(image);
        }

        this.setState({
            imagePreviews
        });

        this.props.addCarImage(image);
    }

    removeImageFromPreviews(imageName) {
        const imagePreviews = this.state.imagePreviews.filter(img => img.name !== imageName);

        this.setState({
            imagePreviews
        });

        this.props.removeCarImage(imageName);
    }

    getImagePreview(image) {
        return (
            <img src={image.url} key={image.name} height="100" className="p-1"
                 onClick={() => this.removeImageFromPreviews(image.name)} alt={image.name}/>
        );
    }

    getImagePreviewBox() {
        return (
            <div>
                <p><b>Fényképek az autóról:</b></p>
                {this.state.imagePreviews.map(image => this.getImagePreview(image))}
            </div>
        );
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        <form>
                            <div className="border p-2 mt-4 mb-4">
                                <h1 className="p-2 m-0 preview-title">Hirdetés előnézete</h1>
                                <div className="car-image-previews mt-4">
                                    {this.props.selectedBrand && <p>Autó márkája: <b>{this.props.selectedBrand}</b></p>}
                                    {this.props.selectedModel && <p>Autó típusa: <b>{this.props.selectedModel}</b></p>}
                                    {this.props.prize && <p>Autó ára: <b>{this.props.prize}</b></p>}
                                    {this.props.description && <p>Leírás: <b>{this.props.description}</b></p>}
                                    {!_.isEmpty(this.state.imagePreviews) && this.getImagePreviewBox()}
                                </div>
                            </div>
                            <h1 className="mt-4 mb-4">Új autó feltöltése</h1>
                            <DoubleSelectField/>
                            <NumberInputField name='car-prize' label='Ár (HUF)' onChange={this.onChangeCarPrize}/>
                            <TextAreaField name='car-desciption' label='Leírás' onChange={this.onChangeCarDescription}/>
                            <CheckBoxFields name='fetures' label='Felszereltség' values={this.state.features}/>
                            <CarImageDropField multiple addImageToPreviews={this.addImageToPreviews}/>
                            <Button/>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        carImages: state.addNewCar.carImages,
        selectedModel: state.cars.selectedModel,
        selectedBrand: state.cars.selectedBrand,
        prize: state.addNewCar.prize,
        description: state.addNewCar.description
    }
}

export default connect(mapStateToProps, {addCarImage, removeCarImage, setCarPrize, setCarDescription})(AddNewCarPage);