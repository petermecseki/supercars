import {GET_FAVOURITE_CARS, UPDATE_FAVOURITE_CARS} from "../actions/types";

const initState = {
    cars: null
}

export default function(state = initState, action) {
    switch(action.type) {
        case GET_FAVOURITE_CARS: {
            return {...state, cars: action.payload};
        }
        case UPDATE_FAVOURITE_CARS: {
            let cars = state.cars;

            if (action.direction === 'ADD') {
                cars.push(action.payload);
            } else {
                cars = cars.filter(car => {
                    return car.id !== action.payload.id;
                });
            }

            return {...state, cars: cars};
        }
        default:
            return state;
    }
}