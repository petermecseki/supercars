import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

import {logOut} from '../../login/actions/index';

class MenuBar extends Component {

    render() {
        if (this.props.router.location.pathname === '/login') {
            return null;
        } else {
            return (
                <div className="container bg-light">
                    <nav className="navbar navbar-expand-lg navbar-light">
                        <a className="navbar-brand" to="/">SuperCars</a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav mr-auto">
                                <li className="nav-item active">
                                    <Link className="nav-link" to="/">Autók <span
                                        className="sr-only">(current)</span></Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/upload">Új autó feltöltése</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/favourites">Elmentett autóim</Link>
                                </li>
                            </ul>
                            <form className="form-inline">
                                <span className="navbar-text ml-4 logOutLink"
                                      onClick={this.props.logOut}>kijelentkezés</span>
                            </form>
                        </div>
                    </nav>
                </div>
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        router: state.router
    }
}

export default connect(mapStateToProps, {logOut})(MenuBar);