import React, {Component, Fragment} from 'react';
import {Route, Switch} from 'react-router-dom';

import Search from './search/components/Search';
import AddNewCarPage from "./addNewCar/components/AddNewCarPage";
import LoginPage from './login/components/LoginPage';

import AuthenticateUser from './login/components/Authenticator';
import MenuBar from './menu/component/MenuBar';
import FavouriteCarsPage from "./favourites/components/FavouriteCarsPage";
import MyCarsPage from "./myCars/components/MyCarsPage";
import CarDetailsPage from "./carDetails/components/CarDetailsPage";

export default class App extends Component {

    render() {
        return (
            <Fragment>
                <MenuBar/>
                <Switch>
                    <Route exact={true} path='/' component={Search}/>
                    <Route path='/car/:id' component={CarDetailsPage}/>
                    <Route path='/upload' component={AuthenticateUser(AddNewCarPage)}/>
                    <Route path='/login' component={LoginPage}/>
                    <Route path='/favourites' component={FavouriteCarsPage}/>
                    <Route path='/mycars' component={MyCarsPage}/>
                </Switch>
            </Fragment>
        );
    }
}
