import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import carReducer from './search/reducers';
import carDetailsReducer from './carDetails/reducers';
import addNewCarReducer from './addNewCar/reducers/addNewCar';
import loginReducer from './login/reducers/index';
import favouriteReducer from './favourites/reducers';
import filterReducer from './search/reducers/filter';

export default (history) => combineReducers({
    router: connectRouter(history),
    cars: carReducer,
    car: carDetailsReducer,
    addNewCar: addNewCarReducer,
    login: loginReducer,
    favourite: favouriteReducer,
    filter: filterReducer
});