import {get, post} from 'axios';

import {
    GET_ALL_CARS, GET_ALL_CAR_BRANDS, GET_MODELS_BY_BRAND, SET_SELECTED_CAR_BRAND, SET_SELECTED_CAR_MODEL,
    FILTER_CARS, SET_FILTER_DEFAULTS, SET_ACTIVE_PAGE, SET_SELECTED_BRAND_AND_MODEL, FAVOURITE_CARS,
    CHANGE_FEATURES_VALUES, GET_ALL_FEATURES
} from './types';
import {
    GET_ALL_CARS_URL,
    GET_ALL_CAR_BRANDS_URL,
    GET_MODELS_BY_BRAND_URL,
    FILTER_CARS_URL,
    REFRESH_CARS_URL,
    GET_ALL_FEATURES_URL
} from './urls';

export function getAllCars() {
    return async dispatch => {
        return new Promise(function (resolve, reject) {
            get(
                GET_ALL_CARS_URL,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            ).then(response => {
                return resolve(dispatch(loadAllCars(response.data)));
            }).catch((error) => {
                return reject(error);
            });
        });
    }
}

export function getAllCarBrands() {
    return async dispatch => {
        return new Promise(function (resolve, reject) {
            get(
                GET_ALL_CAR_BRANDS_URL,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            ).then(response => {
                return resolve(dispatch(loadAllCarBrands(response.data)));
            }).catch((error) => {
                return reject(error);
            });
        });
    }
}

export function getModelsByBrand(brand) {
    return async dispatch => {
        return new Promise(function (resolve, reject) {
            const url = GET_MODELS_BY_BRAND_URL + brand;

            get(
                url,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            ).then(response => {
                return resolve(dispatch(loadAllModelsByBrand(response.data)));
            }).catch((error) => {
                return reject(error);
            });
        });
    }
}

export function changeSelectedCarBrand(brand) {
    return async dispatch => {
        return new Promise(function (resolve, reject) {
            dispatch(setSelectedCarModel(null));
            return resolve(dispatch(setSelectedCarBrand(brand)));
        });
    }
}

export function setSelectedCarBrand(brand) {
    return {
        type: SET_SELECTED_CAR_BRAND,
        payload: brand
    }
}

export function filterCars() {
    return async (dispatch, getState) => {
        return new Promise(function (resolve, reject) {
            const {cars} = getState();
            const model = !cars.selectedModel ? "all" : cars.selectedModel;
            let filterData = `{ "brand": "${cars.selectedBrand}", "model": "${model}"}`;

            post(
                FILTER_CARS_URL,
                JSON.parse(filterData),
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            ).then(response => {
                return resolve(dispatch(loadAllCars(response.data)));
            }).catch((error) => {
                return reject(error);
            });
        });
    }
}

export function setFilterDefaults(filters) {
    return {
        type: SET_FILTER_DEFAULTS,
        payload: filters
    }
}

export function setSelectedCarModel(model) {
    return {
        type: SET_SELECTED_CAR_MODEL,
        payload: model
    }
}

function loadAllModelsByBrand(models) {
    return {
        type: GET_MODELS_BY_BRAND,
        payload: models
    }
}


function loadAllCarBrands(brands) {
    return {
        type: GET_ALL_CAR_BRANDS,
        payload: brands
    }
}

export function setActivePage(page) {
    return async dispatch => {
        dispatch({
            type: SET_ACTIVE_PAGE,
            payload: page
        })
    }
}

export function setSelectedBrandAndModel(selectedBrandAndModel) {
    return async dispatch => {
        dispatch({
            type: SET_SELECTED_BRAND_AND_MODEL,
            payload: selectedBrandAndModel
        });
    }
}

export function addCarToFavourites(car) {
    return async dispatch => {
        dispatch({
            type: FAVOURITE_CARS,
            payload: car
        });
    }
}

export function changeFeaturesValues(value) {
    return async dispatch => {
        dispatch({
            type: CHANGE_FEATURES_VALUES,
            payload: value
        });
    }
}

export function refreshCars(brand, model, features, activePage2) {
    return async (dispatch, getState) => {
        return new Promise(function (resolve, reject) {
            const activePage = activePage2 > 0 ? activePage2 - 1 : activePage2;
            const featureIds = _.isEmpty(features) ? [] : features.map(feature => feature.id);
            let url = "";

            if (!_.isNull(model) && !_.isNull(brand) && !_.isUndefined(model) && !_.isUndefined(brand)) {
                url = REFRESH_CARS_URL + "?brand=" + brand + "&model=" + model + `&page=${activePage}&sort=price,desc`;
            } else if (_.isNull(model) && !_.isUndefined(brand) && !_.isNull(brand)) {
                url = REFRESH_CARS_URL + "?brand=" + brand + "&model=" + `&page=${activePage}&sort=price,desc`;
            } else {
                url = REFRESH_CARS_URL + "?brand=" + "&model=" + `&page=${activePage}&sort=price,desc`;
            }

            post(
                url,
                featureIds,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            ).then(response => {
                dispatch(setActivePage(activePage));
                resolve(dispatch(loadAllCars(response.data)));
            }).catch((error) => {
                return reject(error);
            });
        });
    }
}

function loadAllCars(cars, activePage) {
    return {
        type: GET_ALL_CARS,
        payload: cars
    }
}

export function getAllFeatures() {
    return async dispatch => {
        return new Promise(function (resolve, reject) {
            get(
                GET_ALL_FEATURES_URL,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            ).then(response => {
                return resolve(dispatch(loadAllFeatures(response.data)));
            }).catch((error) => {
                return reject(error);
            });
        });
    }
}

function loadAllFeatures(features) {
    return {
        type: GET_ALL_FEATURES,
        payload: features
    };
}