import React, {Component} from 'react';
import {Link} from "react-router-dom";

import {connect} from "react-redux";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import moment from 'moment';
import FavouriteButton from './FavouriteButton';

import {updateCarFromFavourites} from '../../favourites/actions/index';

class CarBox extends Component {

    constructor(props) {
        super(props);

        this.state = {
            favourites: [],
            selectedFavourite: null,
            newFavouriteAdded: false,
            isFavouriteCar: false
        }

        this.updateFavourite = this.updateFavourite.bind(this);
    }

    componentDidMount() {
        const id = this.props.car.id;
        const carFromFavouriteCars = this.props.favourites.filter(favouriteCar => {
            return favouriteCar.id === id;
        });

        const isFavouriteCar = carFromFavouriteCars.length > 0 ? true : false;
        this.setState({
            isFavouriteCar
        });
    }

    updateFavourite(carId) {
        this.props.updateCarFromFavourites(carId).then(result => {
            const id = this.props.car.id;
            const ize = this.props.favourites.filter(favouriteCar => {
                return favouriteCar.id === id;
            });
            const isFavouriteCar = ize.length > 0 ? true : false;

            this.setState({
                isFavouriteCar
            });
        });
    }

    render() {
        const car = this.props.car;
        const url = '/car/' + car.id;
        const firstRegistration = moment(car.firstRegistration);
        const kilometers = new Intl.NumberFormat('en-IN', {style: 'decimal'}).format(car.kilometer).replace(',', ' ');
        const prize = new Intl.NumberFormat('hu-HU', {style: 'decimal'}).format(car.price);
        const carName = car.brand + ' ' + car.model;

        const boxClassName = this.state.isFavouriteCar ? "row box mt-2 mb-2 rounded favourite" : "row box mt-2 mb-2 rounded";

        return (
            <div className={boxClassName} key={car.id}>
                <div className="col-12 col-md-3 p-2">
                    <Link to={url}>
                        <img
                            src='https://hips.hearstapps.com/amv-prod-cad-assets.s3.amazonaws.com/images/15q4/662479/2016-aston-martin-db9-gt-first-drive-review-car-and-driver-photo-662971-s-original.jpg'
                            className='img-fluid'/>
                    </Link>
                </div>
                <div className="col-12 col-md pt-1 pb-0 pl-2 pr-2">
                    <Link to={url}><h1>{carName}</h1></Link>
                    <p>
                        <span>{firstRegistration.format('YYYY')}/{firstRegistration.format('M')}</span>
                        <span>{car.engine.engineCapacity} cm<sup>3</sup></span>
                        <span>{car.engine.fuelType}</span>
                        <span>{car.engine.power} LE</span>
                        <span>{kilometers} km</span>
                    </p>
                    <p>
                        {car.details}
                    </p>
                </div>
                <div className="col-12 col-md-2 pt-1 pl-2 pl-md-0 pr-2 pb-2 pb-md-0 text-left text-md-right h-100">
                    <div className="row">
                        <div className="col">
                            {prize} Ft
                        </div>
                    </div>
                    <div className="row">
                        <div className="col pt-4">
                            <FavouriteButton car={car} updateFavourite={() => this.updateFavourite(car.id)}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        selectedBrand: state.cars.selectedBrand,
        cars: state.cars.cars,
        favourites: state.favourite.cars
    }
}

export default connect(mapStateToProps, {updateCarFromFavourites})(CarBox);