import React, {Component} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faStar} from "@fortawesome/free-solid-svg-icons";

export default class FavouriteButton extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedFavourite: null
        }

        this.updateFavourites = this.updateFavourites.bind(this);
    }

    updateFavourites() {
        this.props.updateFavourite(this.props.car);
    }

    render() {
        return (
            <span className="favourite-botton mr-1"
                  onClick={() => this.updateFavourites()}>
                <FontAwesomeIcon size="2x" icon={faStar}/>
            </span>
        );
    }

}