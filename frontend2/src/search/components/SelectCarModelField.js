import React, {Component} from 'react';
import {connect} from 'react-redux';

import {setSelectedCarModel} from '../actions/index';

class SelectCarModelField extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedModel: null
        }
    }

    changeModel(selectedModel) {
        this.setState({
            selectedModel
        });
        this.props.setSelectedCarModel(selectedModel);
    }

    selectedValue() {
        return !this.state.selectedModel || !this.props.selectedModel ? "empty" : this.state.selectedModel;
    }

    getField() {
        return (
            <select value={this.selectedValue()} className='form-control mt-2 mb-2'
                    onChange={(event) => this.changeModel(event.target.value)}>
                <option value="empty" disabled hidden>Válasszon típust</option>
                <option value="all">Összes</option>
                {
                    this.props.models && this.props.models.map((model) => {
                        return <option key={model}>{model}</option>
                    })
                }
            </select>
        )
    }

    render() {
        return this.getField();
    }
}

function mapStateToProps(state) {
    return {
        selectedBrand: state.cars.selectedBrand,
        selectedModel: state.cars.selectedModel,
        models: state.cars.models
    }
}

export default connect(mapStateToProps, {setSelectedCarModel})(SelectCarModelField);