import React, {Component} from 'react';
import {connect} from 'react-redux';

import {getAllCarBrands, changeSelectedCarBrand, getModelsByBrand} from '../actions/index';

class SelectField extends Component {

    constructor(props) {
        super(props);

        this.state = {
            carTypes: null,
            selectedCarType: null,
        }

        this.changeCarType = this.changeCarType.bind(this);
    }

    componentDidMount() {
        this.props.getAllCarBrands().then(response => {
            this.setState({
                carTypes: response
            });
        });
    }

    changeCarType(target) {
        this.setState({
            selectedCarType: target.value
        });
        this.props.selectCarType(target.value);
        this.props.changeSelectedCarBrand(target.value);
        this.props.getModelsByBrand(target.value);
    }

    selectedValue() {
        return !this.state.selectedCarType ? "empty" : this.state.selectedCarType;
    }

    getCarTypeSelect() {
        return (
            <select value={this.selectedValue()} className="form-control mt-2 mb-2"
                    onChange={(event) => this.changeCarType(event.target)}>
                <option value="" disabled hidden>Válasszon márkát</option>
                {this.props.brands &&
                this.props.brands.map((carType) => {
                    return <option key={carType} value={carType}>{carType}</option>
                })
                }
            </select>
        );
    }


    render() {
        if (this.props.brands) {
            return this.getCarTypeSelect();
        } else {
            return null;
        }

    }

}

function mapStateToProps(state) {
    return {
        selectedBrand: state.cars.selectedBrand,
        brands: state.cars.brands
    }
}

export default connect(mapStateToProps, {getModelsByBrand, getAllCarBrands, changeSelectedCarBrand})(SelectField);