import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import {login, registration, redirectToHome} from "../actions";
import mapStateToProps from 'react-redux/es/connect/mapStateToProps';

import _ from 'lodash';

class LoginPage extends Component {

    constructor(props) {
        super(props);

        this.LOGIN = 'LOGIN';
        this.REGISTER = 'REGISTER';
        this.ERROR = 'bg-danger';
        this.OK = 'bg-info';

        this.state = {
            formType: this.LOGIN,
            username: null,
            usernameOrEmail: null,
            email: null,
            password: null,
            passwordAgain: null,
            message: null
        }
    }

    changeFormType() {
        this.setState({
            username: null,
            usernameOrEmail: null,
            email: null,
            password: null,
            passwordAgain: null,
            message: null,
            formType: this.state.formType == this.LOGIN ? this.REGISTER : this.LOGIN
        });
        this.changePassword("");
    }

    isLoginForm() {
        return this.state.formType == this.LOGIN;
    }

    getFormFields() {
        let fields = [];

        if (this.isLoginForm()) {
            fields.push(
                <input type="text"
                       onChange={(event) => this.changeUsernameOrEmail(event.target.value)}
                       className="form-control"
                       aria-describedby="usernameOrEmail"
                       placeholder="Felhasználónév vagy email"
                       key="usernameOrEmail"
                       autoComplete="usernameOrEmail"/>
            );
        }

        if (!this.isLoginForm()) {
            fields.push(
                <input type="text"
                       onChange={(event) => this.changeUsername(event.target.value)}
                       className="form-control"
                       aria-describedby="username"
                       placeholder="Felhasználónév"
                       key="username"
                       autoComplete="username"/>
            );
        }

        if (!this.isLoginForm()) {
            fields.push(
                <input type="email"
                       onChange={(event) => this.changeEmail(event.target.value)}
                       className="form-control"
                       aria-describedby="email"
                       placeholder="Email cím"
                       key="email"
                       autoComplete="email"/>
            );
        }

        fields.push(
            <input type="password"
                   onChange={(event) => this.changePassword(event.target.value)}
                   className="form-control"
                   aria-describedby="password"
                   placeholder="Jelszó"
                   key="password"
                   autoComplete="password"/>
        );

        if (!this.isLoginForm()) {
            fields.push(
                <input type="password"
                       onChange={(event) => this.changePasswordAgain(event.target.value)}
                       className="form-control"
                       aria-describedby="password-again"
                       placeholder="Jelszó újra"
                       key="password-again"
                       autoComplete="password-again"/>
            );
        }

        return fields;
    }

    getFormSubmitButton() {
        const style = "btn btn-primary btn-block mt-4 mb-4";

        if (this.isLoginForm()) {
            return <button className={style} onClick={(event) => this.login(event)}>Bejelentkezés</button>;
        } else {
            return <button className={style} onClick={(event) => this.registration(event)}>Regisztráció</button>;
        }
    }

    getChangeFormTypeLink() {
        if (this.isLoginForm()) {
            return <p onClick={() => this.changeFormType()}>Regisztráció</p>;
        } else {
            return <p onClick={() => this.changeFormType()}>Bejelentkezés</p>;
        }
    }

    getFormTitle() {
        const style = "h4 mb-3 font-weight-normal text-center mt-4 mb-4";
        return <h1 className={style}>{this.isLoginForm() ? "Bejelentkezés" : "Regisztráció"}</h1>;
    }

    getForm() {
        return (
            <div className="form-group">
                {this.getFormTitle()}
                {this.getFormFields()}
                {this.getFormSubmitButton()}
                {this.getChangeFormTypeLink()}
            </div>
        );
    }

    changeUsername(username) {
        this.resetMessage();
        if (username.length < 3) {
            this.changeMessage("A felhasználónévnek legalább 3 karakternek kell lennie", this.ERROR);
        }
        this.setState({
            username
        });
    }

    changeUsernameOrEmail(usernameOrEmail) {
        this.resetMessage();
        this.setState({
            usernameOrEmail
        });
    }

    changeEmail(email) {
        this.resetMessage();

        if (_.isNull(this.state.email) || this.state.email.length < 5) {
            this.changeMessage("Az email címnek legalább 5 karakternek kell lennie", this.ERROR);
        } else if (!this.validateEmailAddress(this.state.email)) {
            this.changeMessage("Az email cím érvénytelen", this.ERROR);
        }

        this.setState({
            email
        });
    }

    validateEmailAddress(email) {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
    }

    changePassword(password) {
        this.resetMessage();
        if (_.isNull(this.state.password) || this.state.password.length < 5) {
            this.changeMessage("A jelszónak legalább 5 karakternek kell lennie", this.ERROR);
        }
        this.setState({
            password
        });
    }

    changePasswordAgain(passwordAgain) {
        this.resetMessage();
        if (_.isNull(this.state.passwordAgain) || this.state.passwordAgain.length < 5) {
            this.changeMessage("A megismételt jelszónak legalább 5 karakternek kell lennie", this.ERROR);
        } else if (passwordAgain !== this.state.password) {
            this.changeMessage("A két jelszó nem egyezik", this.ERROR);
        }
        this.setState({
            passwordAgain
        });
    }

    registration(event) {
        event.preventDefault();

        if (_.isNull(this.state.username) || _.isNull(this.state.email) || _.isNull(this.state.password) || _.isNull(this.state.passwordAgain)) {
            this.changeMessage("Minden mező kitöltése kötelező!", this.ERROR);
        } else {
            const registrationData = {
                username: this.state.username,
                email: this.state.email,
                password: this.state.password,
            }
            this.props.registration(registrationData).then(response => {
                this.changeMessage(response.message, this.OK);
            });
        }
    }

    login(event) {
        event.preventDefault();

        if (_.isNull(this.state.usernameOrEmail) || _.isNull(this.state.password)) {
            this.changeMessage("Minden mező kitöltése kötelező!", this.ERROR);
        } else {
            const loginData = {
                usernameOrEmail: this.state.usernameOrEmail,
                password: this.state.password
            }
            this.props.login(loginData).then(response => {
                this.props.redirectToHome();
            }).catch(error => {
            });
        }
    }

    resetMessage() {
        this.setState({
            message: null
        });
    }

    changeMessage(message, type) {
        this.setState({
            message: {
                text: message,
                color: type == this.ERROR ? "error-box p-2 " + this.ERROR : "p-2" + this.OK
            }
        });
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col"></div>
                    <div className="col-auto mt-4" id="box">
                        {!_.isNull(this.state.message) &&
                        <div className={this.state.message.color}>
                            {this.state.message.text}
                        </div>
                        }
                        <form autoComplete="off">
                            {this.getForm()}
                        </form>
                    </div>
                    <div className="col"></div>
                </div>
            </div>
        );
    }
}

export default connect(null, {login, registration, redirectToHome})(LoginPage);