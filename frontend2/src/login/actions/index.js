import {get, post} from 'axios';

import {REGISTRATION_URL, LOGIN_URL} from "./urls";

import {SET_TOKEN} from './types';

import {history} from '../../store';

export function registration(registrationData) {
    return async dispatch => {
        return new Promise(function (resolve, reject) {
            post(
                REGISTRATION_URL,
                registrationData,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            ).then(response => {
                return resolve(response.data);
            }).catch((error) => {
                return reject(error);
            });
        });
    }
}

export function login(loginData) {
    return async dispatch => {
        return new Promise(function (resolve, reject) {
            post(
                LOGIN_URL,
                loginData,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            ).then(response => {
                dispatch(setToken(response.data.accessToken));
                return resolve('OK');
            }).catch((error) => {
                return reject(error);
            });
        });
    }
}

function setToken(token) {
    localStorage.setItem("token", token);
    return {
        type: SET_TOKEN,
        payload: token
    }
}

export function authenticateUser() {
    const token = localStorage.getItem("token");

    if (token !== null) {
        return {
            status: 'OK',
            token
        }
    } else {
        redirectToLoginPage();
        return {
            status: 'ERROR'
        }
    }
}

export function redirectToHome() {
    history.push('/');
}

function redirectToLoginPage() {
    history.push('/login');
}

export function logOut() {
    return async dispatch => {
        if (localStorage.getItem('token')) {
            localStorage.removeItem('token');
            authenticateUser();
        }
    }
}