import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Link } from 'react-router-dom';

import {getCarDetails} from '../actions';
import moment from "moment";

class CarDetailsPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            images: [
                'https://hips.hearstapps.com/amv-prod-cad-assets.s3.amazonaws.com/images/15q4/662479/2016-aston-martin-db9-gt-first-drive-review-car-and-driver-photo-662971-s-original.jpg',
                'https://images.pexels.com/photos/358070/pexels-photo-358070.jpeg?auto=compress&cs=tinysrgb&h=650&w=940',
                'https://images.pexels.com/photos/163415/mercedes-sls-sports-car-luxury-automotive-163415.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
                'https://images.pexels.com/photos/163706/corvette-vette-viper-black-163706.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'
            ]
        }
    }

    componentDidMount() {
        this.props.getCarDetails(this.props.match.params.id);
    }

    render() {
        if(this.props.car) {
            const price = new Intl.NumberFormat('hu-HU', {style: 'decimal'}).format(this.props.car.price);
            const firstRegistration = moment(this.props.firstRegistration);

            return(
                <div className='container' id='car-details'>
                    <div className="row">
                        <div className="col pl-1 pr-1">
                            <Link to=''>Vissza</Link>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col pt-4 pl-1 pr-1">
                            <h1>{this.props.car.brand} {this.props.car.model}</h1>
                        </div>
                    </div>
                    <div className='row'>
                        <div className="col pl-1 pr-1">
                            <img src={this.state.images[0]}
                                 className='img-fluid' />
                        </div>
                    </div>
                    <div className="row">
                            {
                                this.state.images.map((image,index) => {
                                    return <div className="col-3 p-1 image" key={index} alt=""><img src={image} className='img-fluid' /></div>
                                })
                            }
                    </div>
                    <div className="row">
                        <div className="col pl-1 pr-1">
                            <div className="box">
                                Évjárat: { firstRegistration.format('YYYY')}/{firstRegistration.format('M') }
                            </div>
                            <div className="box">
                                Ár: {price} Ft
                            </div>
                            <div className="box">
                                <ul>
                                    <li>{this.props.car.color}</li>
                                    <li>{this.props.car.bodyType}</li>
                                    <li>{this.props.car.kilometer}</li>
                                </ul>
                            </div>
                            <div className="box">
                                Leírás: {this.props.car.details}
                                Hengerlekapcsolás (terheléstől függően 2, 3, 4, henger lekapcsolása), SH-AWD intelligens 4 kerék hajtás, IDS system (menet tulajdonság választó),
                                LED projektoros világítás, LED ködfényszóró, Oldal fellépő (króm), Tetősín (króm), Csomagtartó keresztrúd, Vonóhorog (levehető), Fogadófény,
                                Hátsó fedélzeti multimédia rendszer (tető LCD, 2 db vezeték nélküli fejhallgató), Kulcs nélküli indítás-nyitás-zárás, LED belső világítás,
                                Fűthető kormánykerék, Fűthető hátsó ülések, Aktív parkolást segítő tolató kamera, Elektromos csomagtérajtó mozgatás, Csomagtérszőnyeg
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            return <div>Loading...</div>
        }
    }

}

function mapStateToProps(state) {
    return {
        car: state.car.car
    }
}

export default connect(mapStateToProps, {getCarDetails})(CarDetailsPage);