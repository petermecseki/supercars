import {GET_CARS, GET_CAR_DETAILS} from "./types";
import {get} from "axios";
import {GET_ALL_CARS_URL, GET_CAR_DETAILS_URL} from "../../search/actions/urls";

export function getCars() {
    return async dispatch => {
        const cars = [
            {
                name: 'ASTON MARTIN DB9',
                prize: '12.656.000'
            },
            {
                name: 'ASTON MARTIN DB11',
                prize: '10.000.000'
            }
        ];

        return dispatch(loadCars(cars));
    }
}

export function getCarDetails(carId) {
    return async dispatch => {
        return new Promise(function (resolve, reject) {
            const url = GET_CAR_DETAILS_URL + carId;

            get(
                url,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            ).then(response => {
                return resolve(dispatch(loadCar(response.data)));
            }).catch((error) => {
                return reject(error);
            });
        });
    }
}

function loadCars(cars) {
    return {
        type: GET_CARS,
        payload: cars
    }
}

function loadCar(car) {
    return {
        type: GET_CAR_DETAILS,
        payload: car
    }
}